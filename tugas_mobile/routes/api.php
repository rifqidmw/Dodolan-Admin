<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:api');

Route::get('/show_barang', function() {
	$data = App\Barang::all();
	foreach ($data as $val) {
		$res[] = [
            'id_barang' => $val->id_barang,
			'nama_barang' => $val->nama_barang,
            'stok' => $val->stok,
			'kategori' => $val->kategori['kategori'],
			'ket_barang' => $val->ket_barang,
			'harga' => $val->harga,
			'foto' => $val->foto
		];
	}
	
	return response()->json($res, 200);
});

Route::post('/show_barang_k', function(Request $request){
    $data = App\Kategori::where(['id_kategori' => $request->id_kategori])->first();
    $res = [];
    if(!empty($data)){
        foreach ($data->barang()->get() as $val) {
            $res[] = [
                'id_barang' => $val->id_barang,
                'nama_barang' => $val->nama_barang,
                'stok' => $val->stok,
                'kategori' => $val->kategori['kategori'],
                'ket_barang' => $val->ket_barang,
                'harga' => $val->harga,
                'foto' => $val->foto
            ];
        }
        $output = [
            'status' => 200,
            'data' => $res
        ];
    }else{
        $output = [
            'status' => 110,
            'message' => "no data"
        ];
    }

    return response()->json($output);
});

// Route::post('/create_barang', function(Request $request){
// 	$model = new App\Barang();
// 	$model->nama_barang = $request->nama;
// 	$model->id_kategori = $request->id_kategori;
// 	$model->ket_barang = $request->ket;
//     $model->harga = $request->harga;
//     $model->foto = $request->foto;

//     if($model->save()){
//     	$response = [
//     		"message" => "data berhasil disimpan"
//     	];
//     }else{
//     	$response = [
//     		"message" => "data gagal disimpan"
//     	];
//     }

//     return response()->json($response);
// });

Route::get('/show_kategori', function() {
    $data = App\Kategori::all();
    foreach ($data as $val) {
        $res[] = [
            'id_kategori' => $val->id_kategori,
            'kategori' => $val->kategori,
            'foto' => $val->foto
        ];
    }
    
    return response()->json($res, 200);
});

// Route::post('/create_kategori', function(Request $request){
// 	$model = new App\Kategori();
// 	$model->kategori = $request->kategori;
// 	if($model->save()){
//     	$response = [
//             "status" => 200,
//     		"message" => "data berhasil disimpan"
//     	];
//     }else{
//     	$response = [
//             "status" => 100,
//     		"message" => "data gagal disimpan"
//     	];
//     }

//     return response()->json($response);
// });

Route::post('/register', function(Request $request){

    if(!empty($request->nama) or !empty($request->username) or !empty($request->password)){
        $model = new App\Pembeli();
        $model->nama = $request->nama;
        $model->username = $request->username;
        $model->password = $request->password;
        if($model->save()){
            $response = [
                "status" => 200,
                "message" => "data berhasil disimpan"
            ];
        }else{
            $response = [
                "status" => 110,
                "message" => "data gagal disimpan"
            ];
        }
    }else{
        $response = [
            "status" => 100,
            "message" => "terdapat inputan yang kosong"
        ];
    }

    return response()->json($response);
});

Route::post('/login', function(Request $request){
    $model = App\Pembeli::where(['username'=>$request->username, 'password'=>$request->password])->first();
    if(empty($model)){
        $response = [
            "status" => 100,
            "message" => "login gagal"
        ];
    }else{
        $response = [
            "id_pembeli" => $model->id_pembeli,
            "user" => $model->nama,
            "status" => 200,
            "message" => "login berhasil"
        ];
    }

    return response()->json($response);
});

Route::post('/add_keranjang', function(Request $request){
    $cek = App\Keranjang::where(['id_barang' => $request->id_barang])->first();
    if(!empty($cek)){
        $cek->id_pembeli = $request->id_pembeli;
        $cek->id_barang = $request->id_barang;
        $cek->status = $request->status;
        $cek->qty = $request->qty + 1;
         if($cek->update()){
            $response = [
                "status" => 200,
                "message" => "data berhasil dimasukkan keranjang"
            ];
        }else{
            $response = [
                "status" => 110,
                "message" => "data gagal dimasukkan keranjang"
            ];
        }
    }else{
        $model = new App\Keranjang();
        $model->id_pembeli = $request->id_pembeli;
        $model->id_barang = $request->id_barang;
        $model->status = $request->status;
        $model->qty = $request->qty;

        if($model->save()){
            $response = [
                "status" => 200,
                "message" => "data berhasil dimasukkan keranjang"
            ];
        }else{
            $response = [
                "status" => 110,
                "message" => "data gagal dimasukkan keranjang"
            ];
        }
    }

    return response()->json($response);
});

Route::post('/show_keranjang', function(Request $request){
    $model = App\Pembeli::where(['id_pembeli' => $request->id_pembeli])->first();
    $tot = 0;
    $data = [];
    foreach ($model->keranjang as $val) {
        $data[] = [
            "id_barang" => $val->id_barang,
            "foto" => $val->barang['foto'],
            "nama_barang" => $val->barang['nama_barang'],
            "harga" => $val->barang['harga'],
            "qty" => $val->qty,
            "total" => $val->barang['harga'] * $val->qty
        ];
        $tot = $tot + ($val->barang['harga'] * $val->qty);
    }
    $response = [
        "data" => $data,
        "total_keseluruhan" => $tot,
        "status" => 200
    ];
    return response()->json($response);
});

Route::post('/hapus_keranjang', function(Request $request){
    $model = App\Keranjang::where(['id_pembeli' => $request->id_pembeli, 'id_barang' => $request->id_barang])->first();
    if(!empty($model)){
        if($model->delete()){
            $response = [
                "status" => 200,
                "message" => "data berhasil dihapus"
            ];
        }else{
            $response = [
                "status" => 110,
                "message" => "data gagal dihapus"
            ];
        }
    }
    return response()->json($response);
});






