<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/barang', 'BarangController@show')->name('barang');
Route::get('/add_barang', 'BarangController@add');
Route::post('/barang/create', 'BarangController@create');
Route::get('/edit_barang/{id}', 'BarangController@edit');
Route::post('/barang/update', 'BarangController@update');
Route::get('/delete_barang/{id}', 'BarangController@delete');

Route::get('/kategori', 'KategoriController@show')->name('kategori');
Route::post('/kategori/create', 'KategoriController@create');
Route::get('/edit_kategori/{id}', 'KategoriController@show');
Route::post('/kategori/update', 'KategoriController@update');
Route::get('/delete_kategori/{id}', 'KategoriController@delete');
