@extends('layouts/master')
@section('title', $title)
@section('content')
	<div class="panel panel-default" id="data">
		<div class="panel-heading">
			<a href="{{ url('add_barang') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus-circle"></i> tambah</a>
		</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<td>No</td>
						<td>Nama</td>
						<td width="30%">Keterangan</td>
						<td>Stok</td>
						<td>Harga</td>
						<td>foto</td>
						<td>Aksi</td>
					</tr>
				</thead>
				<tbody>
					@foreach($record as $value => $key)
					<tr>
						<td>{{ $value + 1 }}</td>
						<td>{{ $key->nama_barang }}</td>
						<td>{{ $key->ket_barang }}</td>
						<td>{{ $key->stok }}</td>
						<td>{{ $key->harga }}</td>
						<td><img src="{{ asset('images/barang/'.$key->foto) }}" width="125px"></td>
						<td>
							<a href="{{ url('edit_barang/'.$key->id_barang) }}" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o"></i> edit</a>
							<a data-href="{{ URL('/delete_barang/'.$key->id_barang) }}" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalHapus"><i class="fa fa-trash-o"></i> hapus</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="panel-footer">
			 
		</div>
	</div>
@endsection