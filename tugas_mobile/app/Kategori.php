<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table ="kategori";
    public $timestamps = false;
    protected $primaryKey = "id_kategori";
    protected $fillable = ['kategori', 'foto'];

    // public function foto()
    // {
    // 	if($this->attributes['foto']=="")
    // 		return "no image :)";
    // 	else
    // 		return $this->attributes['foto'];
    // }

    public function barang()
    {
        return $this->hasMany('App\Barang', 'id_kategori', 'id_kategori');
    }
}
    