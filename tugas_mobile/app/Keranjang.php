<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model
{
    protected $table ="keranjang";
    public $timestamps = false;
    protected $primaryKey = "id_keranjang";
    protected $fillable = ['id_pembeli', 'id_barang', 'qty'];

    public function barang()
    {
    	return $this->belongsTo('App\Barang', 'id_barang', 'id_barang');
    }
}
