<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use Validator;

class BarangController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
	public function show()
	{
		$record = Barang::all();
		$data = [
			'title' => "Data Barang",
			'page' => "Data Barang",
			'record' => $record
		];
		return view('pages.barang.data', $data);
	}

	public function add()
	{
		$d_kategori = \App\Kategori::all();
		$data = [
			'title' => "Tambah Data Barang",
			'page' => "Tambah Data Barang",
			'kategori' => $d_kategori
		];
		return view('pages.barang.tambah', $data);
	}


    public function validasi($request)
    {
    	return $this->validate($request, [
			'nama' => 'required',
			'kategori' => 'required',
			'ket' => 'required',
			'stok' => 'required|numeric',
			'harga' => 'required|numeric',
			'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
		]);
    }

	public function create(Request $request)
	{
		$this->validasi($request);

		$nama_gambar = $request->file('foto')->getClientOriginalName();
        $model = new Barang();
        $model->nama_barang = $request->nama;
        $model->id_kategori = "1";
        $model->ket_barang = $request->ket;
        $model->stok = $request->stok;
        $model->harga = $request->harga;
        $model->foto = $nama_gambar;
        
        if($model->save()){
			$request->file('foto')->move(
    			base_path() . '/public/images/barang/', $nama_gambar
			);
        }

        return redirect('/barang');   
	}

	public function edit($id)
	{
		$data = [
			'title' => "Edit Data Barang",
			'page' => "Edit Data Barang",
			'record' => Barang::where(['id_barang'=>$id])->first()
		];
		return view('pages.barang.edit', $data);
	}

	public function update(Request $request)
	{
		$this->validate($request, [
			'nama' => 'required',
			'ket' => 'required',
			'stok' => 'required|numeric',
			'harga' => 'required|numeric'
		]);

		$id = $request->id_menu;
		$model = Barang::find($id);
		if(!empty($model)){
			$model->nama_barang = $request->nama;
        	$model->ket_barang = $request->ket;
        	$model->stok = $request->stok;
        	$model->harga = $request->harga;

        	if($request->foto == ""){
        		$model->foto = $request->foto2;
        	}else{
        		$nama_gambar = $request->file('foto')->getClientOriginalName();
				$model->foto = $nama_gambar;
        		$request->file('foto')->move(
    				base_path() . '/public/images/barang/', $nama_gambar);
        	}
        	$model->update();
		}

        return redirect('/barang');
	}

	public function delete($id)
	{
		$hapus = Barang::where(['id_barang'=>$id]);
        if(!empty($hapus)) {
            $hapus->delete();
        }
        return redirect('barang');
	}
}
