<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use Validator;

class KategoriController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id = null)
	{
		$record = Kategori::all();

		if($id != null){
			$data_edit = Kategori::where(['id_kategori' => $id])->first();
		}else{
			$data_edit = "";
		}

		$data = [
			'title' => "Data Kategori",
			'page' => "Data Kategori",
			'r_edit' => $data_edit,
			'record' => $record
		];
		return view('pages.kategori.data', $data);
	}

	public function validasi($request)
	{
		return $this->validate($request, [
			'kategori' => 'required',
			'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
		]);
	}

	public function create(Request $request)
	{
		$this->validasi($request);

		$nama_gambar = $request->file('foto')->getClientOriginalName();
		$model = new Kategori();
		$model->kategori = $request->kategori;
		$model->foto = $nama_gambar;
		if($model->save()){
			$request->file('foto')->move(
    			base_path() . '/public/images/kategori/', $nama_gambar
			);
        }

		return redirect('/kategori');
	}

	public function update(Request $request)
	{
		$this->validasi($request);

		$id = $request->id;
		$model = Kategori::find($id)->first();
		$model->kategori = $request->kategori;

		if(!empty($model)){
			$model->update();
		}
		return redirect('/kategori');
	}

	public function delete($id)
	{
		$hapus = Kategori::where(['id_kategori'=>$id])->first();
        if(!empty($hapus)) {
            $hapus->delete();
        }
        return redirect('/kategori');
	}
}
