<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table ="barang";
    public $timestamps = false;
    protected $primaryKey = "id_barang";
    protected $fillable = ['nama_barang' , 'id_kategori', 'ket_barang', 'harga', 'foto'];

    public function kategori()
    {
        return $this->belongsTo('App\Kategori', 'id_kategori', 'id_kategori');
    }

}
