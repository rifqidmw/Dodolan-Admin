<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembeli extends Model
{
    protected $table ="pembeli";
    public $timestamps = false;
    protected $primaryKey = "id_pembeli";
    protected $fillable = ['nama' , 'username', 'password'];

    public function keranjang()
    {
    	return $this->hasMany('App\Keranjang', 'id_pembeli', 'id_pembeli');
    }
}
