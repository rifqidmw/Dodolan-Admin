@extends('layouts/master')
@section('title', $title)
@section('content')
	<div class="panel panel-default" id="input">
		<div class="panel-heading">
			<a href="{{ url('barang') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left"></i> kembali</a>
		</div>
		<div class="panel-body">
			<form class="form-horizontal" action="{{ url('/barang/create') }}" method="post" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div  class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
					<label class="control-label col-md-2 col-sm-3">Nama Barang :</label>
					<div class="col-md-4 col-sm-5">
						<input type="text" name="nama" class="form-control input-sm" placeholder="masukkan nama barang..." value="{{ old('nama') }}">
					</div>
					<div class="col-md-6">
						@if ($errors->has('nama'))
                            <span class="help-block">
                                {{ $errors->first('nama') }}
                            </span>
                        @endif
					</div>
				</div>
				<div  class="form-group{{ $errors->has('kategori') ? ' has-error' : '' }}">
					<label class="control-label col-md-2 col-sm-3">Kategori Barang :</label>
					<div class="col-md-4 col-sm-5">
						<select name="kategori" class="form-control input-sm">
							<option value="">Pilih Kategori</option>
							@foreach($kategori as $val)
								<option value="{{ $val->id_kategori }}" @if(old('kategori') == $val->id_kategori)selected @endif>{{ $val->kategori }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6">
						@if ($errors->has('kategori'))
                            <span class="help-block">
                                {{ $errors->first('kategori') }}
                            </span>
                        @endif
					</div>
				</div>
				<div  class="form-group{{ $errors->has('ket') ? ' has-error' : '' }}">
					<label class="control-label col-md-2 col-sm-3">Keterangan Barang :</label>
					<div class="col-md-4 col-sm-5">
						<textarea class="form-control input-sm" name="ket" placeholder="masukkan keterangan barang..."></textarea>
					</div>
					<div class="col-md-6">
						@if ($errors->has('ket'))
                            <span class="help-block">
                                {{ $errors->first('ket') }}
                            </span>
                        @endif
					</div>
				</div>
				<div class="form-group{{ $errors->has('stok') ? ' has-error' : '' }}">
					<label class="control-label col-md-2 col-sm-3">Stok :</label>
					<div class="col-md-4 col-sm-5">
						<input type="text" name="stok" class="form-control input-sm" placeholder="masukkan stok barang..." value="{{ old('stok') }}">
					</div>
					<div class="col-md-6">
						@if ($errors->has('stok'))
                            <span class="help-block">
                                {{ $errors->first('stok') }}
                            </span>
                        @endif
					</div>
				</div>
				<div class="form-group{{ $errors->has('harga') ? ' has-error' : '' }}">
					<label class="control-label col-md-2 col-sm-3">Harga :</label>
					<div class="col-md-4 col-sm-5">
						<input type="text" name="harga" class="form-control input-sm" placeholder="masukkan harga barang..." value="{{ old('harga') }}">
					</div>
					<div class="col-md-6">
						@if ($errors->has('harga'))
                            <span class="help-block">
                                {{ $errors->first('harga') }}
                            </span>
                        @endif
					</div>
				</div>
				<div class="form-group{{ $errors->has('foto') ? ' has-error' : '' }}">
					<label class="control-label col-md-2 col-sm-3">Foto :</label>
					<div class="col-md-4 col-sm-5">
						<input type="file" name="foto" class="form-control input-sm">
					</div>
					<div class="col-md-6">
						@if ($errors->has('foto'))
                            <span class="help-block">
                                {{ $errors->first('foto') }}
                            </span>
                        @endif
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-10 col-md-offset-2 col-sm-offset-3">
						<button type="submit" class="btn btn-primary btn-sm">simpan</button>
						<button type="reset" class="btn btn-danger btn-sm">reset</button>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection