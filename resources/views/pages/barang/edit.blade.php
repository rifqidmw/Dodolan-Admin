@extends('layouts/master')
@section('title', $title)
@section('content')
	<div class="panel panel-default" id="input">
		<div class="panel-heading">
			<a href="{{ url('barang') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left"></i> kembali</a>
		</div>
		<div class="panel-body">
			<form class="form-horizontal" action="{{ url('/barang/update') }}" method="post" enctype="multipart/form-data">
				{{ csrf_field() }}
				<input type="hidden" name="id_menu" value="{{ $record->id_barang }}">
				<div  class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
					<label class="control-label col-md-2 col-sm-3">Nama Barang :</label>
					<div class="col-md-4 col-sm-5">
						<input type="text" name="nama" class="form-control input-sm" placeholder="masukkan nama barang..." value="{{ $record->nama_barang }}">
					</div>
					<div class="col-md-6">
						@if ($errors->has('nama'))
                            <span class="help-block">
                                {{ $errors->first('nama') }}
                            </span>
                        @endif
					</div>
				</div>
				<div  class="form-group{{ $errors->has('ket') ? ' has-error' : '' }}">
					<label class="control-label col-md-2 col-sm-3">Keterangan Barang :</label>
					<div class="col-md-4 col-sm-5">
						<textarea class="form-control input-sm" name="ket" placeholder="masukkan keterangan barang...">{{ $record->ket_barang }}
						</textarea>
					</div>
					<div class="col-md-6">
						@if ($errors->has('ket'))
                            <span class="help-block">
                                {{ $errors->first('ket') }}
                            </span>
                        @endif
					</div>
				</div>
				<div class="form-group{{ $errors->has('stok') ? ' has-error' : '' }}">
					<label class="control-label col-md-2 col-sm-3">Stok :</label>
					<div class="col-md-4 col-sm-5">
						<input type="text" name="stok" class="form-control input-sm" placeholder="masukkan stok barang..." value="{{ $record->stok }}">
					</div>
					<div class="col-md-6">
						@if ($errors->has('stok'))
                            <span class="help-block">
                                {{ $errors->first('stok') }}
                            </span>
                        @endif
					</div>
				</div>
				<div class="form-group{{ $errors->has('harga') ? ' has-error' : '' }}">
					<label class="control-label col-md-2 col-sm-3">Harga :</label>
					<div class="col-md-4 col-sm-5">
						<input type="text" name="harga" class="form-control input-sm" placeholder="masukkan harga barang..." value="{{ $record->harga }}">
					</div>
					<div class="col-md-6">
						@if ($errors->has('harga'))
                            <span class="help-block">
                                {{ $errors->first('harga') }}
                            </span>
                        @endif
					</div>
				</div>
				<div class="form-group{{ $errors->has('foto') ? ' has-error' : '' }}">
					<label class="control-label col-md-2 col-sm-3">Foto :</label>
					<div class="col-md-4 col-sm-5">
						<input type="file" name="foto" class="form-control input-sm">
						<input type="hidden" name="foto2" value="{{ $record->foto }}">
						<img src="{{ asset('images/barang/'.$record->foto) }}" width="125px" class="img-thumbnail">
					</div>
					<div class="col-md-6">
						@if ($errors->has('foto'))
                            <span class="help-block">
                                {{ $errors->first('foto') }}
                            </span>
                        @endif
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-10 col-md-offset-2 col-sm-offset-3">
						<button type="submit" class="btn btn-primary btn-sm">edit</button>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection