@extends('layouts/master')
@section('title', $title)
@section('content')
	<div class="panel panel-default" id="input">
		<div class="panel-heading">
			
		</div>
		<div class="panel-body">
			@if($r_edit == null)
			<form class="form-horizontal" action="{{ url('/kategori/create') }}" method="post" enctype="multipart/form-data">
			@else
			<form class="form-horizontal" action="{{ url('/kategori/update') }}" method="post" enctype="multipart/form-data">
			@endif
				{{ csrf_field() }}
				<div class="form-group{{ $errors->has('kategori') ? ' has-error' : '' }}">
					<label class="control-label col-md-2 col-sm-3">Nama Kategori :</label>
					<div class="col-md-4 col-sm-5">
						@if($r_edit == null)
							<input type="text" name="kategori" class="form-control input-sm" placeholder="masukkan nama kategori..." value="{{ old('kategori') }}">
						@else
							<input type="hidden" name="id" value="{{ $r_edit->id_kategori }}">
							<input type="text" name="kategori" class="form-control input-sm" placeholder="masukkan nama kategori..." value="{{ $r_edit->kategori }}">
						@endif
					</div>
					<div class="col-md-6">
						@if ($errors->has('kategori'))
                            <span class="help-block">
                                {{ $errors->first('kategori') }}
                            </span>
                        @endif
					</div>
				</div>
				<div class="form-group{{ $errors->has('foto') ? ' has-error' : '' }}">
					<label class="control-label col-md-2 col-sm-3">Foto :</label>
					<div class="col-md-4 col-sm-5">
						<input type="file" name="foto" class="form-control input-sm">
					</div>
					<div class="col-md-6">
						@if ($errors->has('foto'))
                            <span class="help-block">
                                {{ $errors->first('foto') }}
                            </span>
                        @endif
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-10 col-md-offset-2 col-sm-offset-3">
						@if($r_edit == null)
							<button type="submit" class="btn btn-primary btn-sm">simpan</button>
							<button type="reset" class="btn btn-danger btn-sm">reset</button>
						@else
							<button type="submit" class="btn btn-primary btn-sm">edit</button>
							<a href="{{ url('kategori') }}" class="btn btn-danger btn-sm">batal</a>
						@endif
					</div>
				</div>
			</form>
		</div>
		<div class="panel-footer" id="data">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<td>No</td>
						<td>kategori</td>
						<td>Foto</td>
						<td>Aksi</td>
					</tr>
				</thead>
				<tbody>
					@foreach($record as $value => $key)
					<tr>
						<td>{{ $value + 1 }}</td>
						<td>{{ $key->kategori }}</td>
						<td><img src="{{ asset('images/kategori/'.$key->foto) }}" width="125px"></td>
						<td>
							<a href="{{ url('edit_kategori/'.$key->id_kategori) }}" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o"></i> edit</a>
							<a data-href="{{ URL('/delete_kategori/'.$key->id_kategori) }}" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalHapus"><i class="fa fa-trash-o"></i> hapus</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection