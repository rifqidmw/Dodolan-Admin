<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" rel="stylesheet">
	<style type="text/css">
		html, body{
			margin: 0;
			padding: 0;
		    background: #f2f2f2;
		}
		/*buat nampil sidebar*/
		.side-bar
		{
		    background:#19334d;
		    position:absolute;
		    position: fixed;
		    height:100%;
		    width:200px;
		    color:#fff;
		    transition: margin-left 0.5s;
		}
		.side-bar ul{
			padding: 5px 0px;
		}
		.side-bar ul li{
			list-style: none;
			padding: 1px;
		}
		.side-bar ul li a{
			text-decoration: none;
			font-size: 15px;
			color: #d9d9d9;
			background: #336699;
			display: block;
			padding: 10px 20px;
			border-left: 5px solid #005580;
		}
		.side-bar ul li a i{
			padding-right: 5px;
		}

		/*buat nampil konten*/
		.content
		{
		    padding-left: 200px;
		    transition: padding-left 0.5s;
		}
		.content .isi{
			padding: 5px;
		}
		.content .isi #input label{
			font-weight: 100;
		}
		.content .navbar{
			background: #005580;
			border: none;
		}
		.content .navbar ul li a{
			color: #f2f2f2;
		}
		.content .navbar-brand{
			color: #f2f2f2;
			font-size: 15px;
		}

		.content .logout{
			cursor: pointer;
		}

		/*buat nampil data*/
		#data{
			width: auto;
		}
		#data table{
			width: auto;
		}


	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="side-bar">
				<ul>
					<li><a href="{{ url('home') }}"><i class="fa fa-home"></i>Dashboard</a></li>
					<li><a href=""><i class="fa fa-user"></i>User</a></li>
					<li><a href="{{ route('barang') }}"><i class="fa fa-cutlery"></i>Barang</a></li>
					<li><a href="{{ route('kategori') }}"><i class="fa fa-cutlery"></i>Kategori</a></li>
				</ul>
			</div>
			<div class="content">
				<!-- Navbar -->
				<div class="navbar navbar-inverse navbar-static-top">
				    <div class="container-fluid">
				        <div class="navbar-header">
				            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				            </button>
				            <a class="navbar-brand">{{ $page }}</a>
				        </div>
				        <div class="navbar-collapse collapse">
				            <ul class="nav navbar-nav navbar-right">
				                <li class="dropdown">
				                    <a class="dropdown-toggle" role="button"><i class="fa fa-user-circle"></i> Admin </a>
				                </li>
				                <li><a data-href="{{ url('/logout') }}" data-toggle="modal" data-target="#modalKeluar" class="logout" data-onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> Logout</a></li>
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
				            </ul>
				        </div>
				    </div>
				</div>

				<!-- konten -->
				<div class="isi">
					@yield('content')
				</div>
			</div>
		</div>
	</div>
	<script src="{{ asset('js/app.js') }}"></script>
	<!-- Konfirmasi hapus -->
	<div class="modal fade" id="modalHapus">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="text-danger">PERHATIAN!!!</h4>
				</div>
				<div class="modal-body">
					Apakah anda yakin akan menghapus data ini ?
				</div>
				<div class="modal-footer">
					<a class="btn btn-danger btn-ok btn-sm">Hapus</a>
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
    
    <!-- Konfirmasi keluar -->
	<div class="modal fade" id="modalKeluar">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="text-danger">PERHATIAN!!!</h4>
				</div>
				<div class="modal-body">
					Apakah anda yakin akan keluar  ?
				</div>
				<div class="modal-footer">
					<a class="btn btn-danger btn-ok btn-sm">Keluar</a>
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
    <script type="text/javascript">
	    $(document).ready(function() {
	        $('#modalHapus').on('show.bs.modal', function(e) {
	            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	        });
	        $('#modalKeluar').on('show.bs.modal', function(e) {
	            $(this).find('.btn-ok').attr('onclick', $(e.relatedTarget).data('onclick'));
	        });
	    });
    </script>
</body>
</html>